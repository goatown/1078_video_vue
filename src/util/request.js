import axios from "axios";
import router from "../router/index";
let  baseURL =IPConfig.BASE_WEBURL
 
//创建axios实例
const request = axios.create({
    timeout: 5000,
    baseURL:baseURL
});

//请求拦截器
request.interceptors.request.use(
    config => {
        config.headers.token = localStorage.getItem('token') 
        return config;
    },
    
    error => {Promise.reject(error)}
);

request.interceptors.response.use(
    response => {
        if(response.data=="token过期或者jtKeyId不存在,请重新登陆"){
            localStorage.clear();
            router.push("/")
        }
       
        return response.data;
    },
    error => {
        if (error.response) {
            switch (error.response.status) {
                case 401:
                    localStorage.clear();
                    router.push("/")
                    console.log("401")
            }
        }
        return Promise.reject(error.response)  
         // 返回接口返回的错误信息
    });


 
export default request