import request from '../util/request' // 引入刚刚创建的域名文件
const base = '/instruction';// 解决跨域问题
const api = {
    //登陆
    getLogin(username, password) {
        return request.get(`${base}/user/token`, {
            params: {
                jtkeyId: username,
                jtKeySecret: password
            }
        })
    },
    // 在线车辆
    getCarsList() {
        return request.get(`${base}/user/terminal/carall`)
    },
    //实时视频播放 
    getRealTimeVideo(clientId, channelNo, mediaType, streamType) {
        return request.get(`${base}/media/defaultrealtime/play`, {
            params: {
                clientId: clientId,
                channelNo: channelNo,
                mediaType: mediaType,
                streamType: streamType
            }
        })
    },

    //获取车辆信息 
    getVehicleInfo(clientId) {
        return request.get(`${base}/card/drive_recorder/data`, {
            params: {
                clientId: clientId
            }
        })
    },
    //设置车辆信息 
    setVehicleInfo(clientId , IdentificationCode, PlateNumber, PlateType) {
        return request.put(`${base}/card/drive_recorder/parameters?clientId=`+clientId+
                                         '&IdentificationCode='+IdentificationCode+
                                         "&PlateNumber="+PlateNumber+
                                         "&PlateType="+PlateType)
    },
    //获取驾驶员信息 
    getICInfo(clientId) {
        return request.get(`${base}/card/driver_info/upload`, {
            params: {
                clientId: clientId
            }
        })
    },

    //下发文本
    setText(clientId,content) {
        return request.post(`${base}/card/info/text?clientId=`+clientId+"&content="+content)
    },
    //查询终端音视频属性
    getAVAttribute(clientId) {
            return request.get(`${base}/media/attributes`, {
                params: {
                    clientId: clientId
                }
            })
    },  //查询提取视频
    getUpFile(clientId,channel,startTime,endTime,page,size) {
            return request.get(`${base}/LargeScreen/videoExtraction`, {
                params: {
                    clientId: clientId,
                    channel:channel,
                    startTime:startTime,
                    endTime:endTime,
                    page:page,
                    size:size
                }
            })
    },
    //报警文件记录
    getAlarm(clientId,type,startTime,endTime,page ,size) {
        return request.get(`${base}/LargeScreen/AlarmRecord`, {
            params: {
                clientId: clientId,
                type: type,
               
                startTime: startTime,
                endTime: endTime,
                page:page,
                size:size
            }
        })
     },
     
    //回放视频播放 
    getBackVideo(clientId, channelNo, mediaType, streamType,storageType,playbackMode,playbackSpeed,startTime,endTime) {
        return request.get(`${base}/media/defaulthistory/play`, {
            params: {
                clientId: clientId,
                channelNo: channelNo,
                mediaType: mediaType,
                streamType: streamType,
                storageType: storageType,
                playbackMode: playbackMode,
                playbackSpeed: playbackSpeed,
                startTime: startTime,
                endTime: endTime
            }
        })
    },
    //提取视频文件
    setUploadFile(clientId,path,channelNo,startTime,endTime,warnBit1,warnBit2,mediaType,streamType,storageType,condition ) {
        return request.get(`${base}/media/defaultfile/upload`, {
            params: {
                clientId: clientId,
                path: path,
                channelNo: channelNo,
                startTime: startTime,
                endTime: endTime,
                warnBit1: warnBit1,
                warnBit2: warnBit2,
                mediaType: mediaType,
                streamType: streamType,
                storageType: storageType,
                condition: condition
            }
        })
    },
    //关闭视频
    closeVideo(clientId, channelNo, tag,) {
        return request.get(`${base}/user/video`, {
            params: {
                clientId: clientId,
                channelNo: channelNo,
                tag: tag
            }
        })
    },
    //查询回放录像时间
    selectBackTime(clientId, channelNo, startTime, endTime, warnBit1, warnBit2, mediaType, streamType, storageType) {
        return request.get(`${base}/media/file/search`, {
            params: {
                clientId: clientId,
                channelNo: channelNo,
                startTime: startTime,
                endTime: endTime,
                warnBit1: warnBit1,
                warnBit2: warnBit2,
                mediaType: mediaType,
                streamType: streamType,
                storageType: storageType
            }
        })
    },
    //查询历史回放轨迹
    selectBackPath(clientId, date, startTime, endTime) {
        return request.get(`${base}/user/getHistoricalTrack`, {
            params: {
                clientId: clientId,
                date: date,
                startTime: startTime,
                endTime: endTime
            }
        })
    },
        //查询大屏
    selectLargeScreen() {
            return request.get(`${base}/LargeScreen/data`, {
               
            })
        },
    //Ic卡记录
    selectIcCard(clientId, startTime, endTime,driverName,page,size) {
        return request.get(`${base}/LargeScreen/IcCardRecord`, {
            params: {
                clientId: clientId,
                startTime: startTime,
                endTime: endTime,
                driverName:driverName,
                page:page,
                size:size
            }
        })
    },
    //设备注册情况
    selectRgister(clientId, onlineStatus, page,size) {
        return request.get(`${base}/LargeScreen/device`, {
            params: {
                clientId: clientId,
                onlineStatus: onlineStatus,
                page:page,
                size:size
            }
        })
    },

};
export default api