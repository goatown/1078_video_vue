import { createApp } from 'vue'
import App from './App.vue'
import ployfill from "@/util/ployfill"    // 注意文件路径
import router from './router/index'
import 'element-plus/dist/index.css'
//1.导入组件库
import ElementPlus from 'element-plus';
//2.导入组件样式相关
//3.配置vue相关插件
const app=createApp(App)
import zhCn from 'element-plus/es/locale/lang/zh-cn'
app.use(ElementPlus, {
  locale: zhCn,
})
import VueAMap from 'vue-amap'
import dataV from '@jiaminghi/data-view'
app.config.globalProperties=ployfill;
app.use(dataV)
app.use(VueAMap)
app.use(router)
app.mount('#app')
