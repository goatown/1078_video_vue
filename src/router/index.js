import { createRouter, createWebHashHistory } from "vue-router";
const router = createRouter({
    history: createWebHashHistory("/"),
    routes: [
        {
            name: '首页',
            path: '/report',
            component: () => import('../views/data_report/report.vue'),
            meta: {
                auth: true,
              },
        },
        {
            name: '轨迹回放',
            path: '/trackPlayback',
            component: () => import('../views/track_playback/trackPlayback.vue'),
            meta: {
                auth: true,
              },
        },
        
        {
            name: '登录',
            path: '/',
            component: () => import('../views/login.vue'),
            meta: {
                auth: false,
              },
        },
        
        {
            name: 'rtvs实时',
            path: '/rtvsReal',
            component: () => import('../views/JT_video/rtvsReal.vue'),
            meta: {
                auth: true,
              },
        },
        {
            name: 'rtvs回放',
            path: '/rtvsBack',
            component: () => import('../views/JT_video/rtvsBack.vue'),
            meta: {
                auth: true,
              },
        },
        {
            name: '实时视频',
            path: '/flvReal',
            component: () => import('../views/JT_video/flvReal.vue'),
            meta: {
                auth: true,
              },
        },
        {
            name: '回放视频',
            path: '/flvBack',
            component: () => import('../views/JT_video/flvBack.vue'),
            meta: {
                auth: true,
              },
        },
       
  
       
   

        {
            name: '管理中心',
            path: '/manage',
            component: () => import('../components/manage/manage.vue'),
            meta: {
                auth: true,
              },
              children:[
                {
                    name: '控制台',
                    path: '/control',
                    component: () => import('../views/manage/Control/control.vue'),
                    meta: {
                        auth: true,
                    }, 
                },
                {
                    name: 'IC卡管理',
                    path: '/IcCard',
                    component: () => import('../views/manage/terminal/IcCard.vue'),
                    meta: {
                        auth: true,
                      },
                },
                {
                    name: '终端信息',
                    path: '/terminalInfo',
                    component: () => import('../views/manage/terminal/terminalInfo.vue'),
                    meta: {
                        auth: true,
                      },
                },
                {
                    name: '车辆信息',
                    path: '/vehicleInfo',
                    component: () => import('../views/manage/terminal/vehicleInfo.vue'),
                    meta: {
                        auth: true,
                      },
                },
                {
                    name: '提取视频中心',
                    path: '/videoCenter',
                    component: () => import('../views/manage/terminal/videoCenter.vue'),
                    meta: {
                        auth: true,
                      },
                },
               {
                    name: '高级驾驶辅助系统报警',
                    path: '/vehicle_0x64',
                    component: () => import('../views/manage/vehicle/vehicle_0x64.vue'),
                    meta: {
                        auth: true,
                      },
                },{
                    name: '驾驶员状态监测系统报警',
                    path: '/vehicle_0x65',
                    component: () => import('../views/manage/vehicle/vehicle_0x65.vue'),
                    meta: {
                        auth: true,
                      },
                },{
                    name: '胎压监测系统报警',
                    path: '/vehicle_0x66',
                    component: () => import('../views/manage/vehicle/vehicle_0x66.vue'),
                    meta: {
                        auth: true,
                      },
                },{
                    name: '盲区监测系统报警',
                    path: '/vehicle_0x67',
                    component: () => import('../views/manage/vehicle/vehicle_0x67.vue'),
                    meta: {
                        auth: true,
                      },
                }
               
              ]
        },


    ],
});
router.beforeEach((to, from, next) => {
    // 1. 每个条件执行后都要跟上 next() 或 使用路由跳转 api 否则页面就会停留一动不动
    // 2. 要合理的搭配条件语句，避免出现路由死循环。
    const token = localStorage.getItem('token')
    
    if(to.path=="/"&&token!=null){
        router.replace("/report")
        next()
    }
    else if (to.meta.auth&&token==null){
        router.replace("/")
        next()
    } else{
        next()
    }
   
})


export default router;
